<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



$api = app('Dingo\Api\Routing\Router');
$api->version('v2', function ($api) {
      //Front end url
      $api->post('city_details', 'App\Http\Controllers\Api\Common@getcity_categories');
      $api->post('signup_user', 'App\Http\Controllers\Api\Common@signup_user');
      $api->post('verify_user', 'App\Http\Controllers\Api\Common@verify_user');
      //    $api->post('verify_user_old', 'App\Http\Controllers\Api\Common@verify_user_old');
      $api->post('resend_otp', 'App\Http\Controllers\Api\Common@resend_otp');
      $api->post('update_user', 'App\Http\Controllers\Api\Common@update_user'); 
      $api->post('postcontactus', 'App\Http\Controllers\Api\Common@storecontact');
      $api->post('signin_user', 'App\Http\Controllers\Api\Common@signin_user');
      $api->post('forgot_password', 'App\Http\Controllers\Api\Common@forgot_password'); 
      $api->post('view-hotel/{url_index}','App\Http\Controllers\Api\Common@view_property');
      $api->post('viewhotel/review_comments','App\Http\Controllers\Api\Common@review_comments');
      $api->post('roomshow','App\Http\Controllers\Api\Common@roomshow');
      $api->post('roomsavailablitycheck','App\Http\Controllers\Api\Common@roomsavailablitycheck');
      $api->post('roomtypeavailablitycheck','App\Http\Controllers\Api\Common@roomtypeavailablitycheck');
      $api->get('list-hotels/city/{url_index}','App\Http\Controllers\Api\Common@city_property_listing');
     $api->get('list-hotels/category/{url_index}','App\Http\Controllers\Api\Common@category_property_listing');
      $api->post('subscription_payment_response','App\Http\Controllers\Api\Common@payment_response');
      $api->post('payment_response_subscription','App\Http\Controllers\Api\Common@paymentResponse');
      $api->post('wallet_payment_response','App\Http\Controllers\Api\Common@wallet_payment_response');
      $api->post('payment_response_wallet','App\Http\Controllers\Api\Common@payment_response_wallet');      
      // $api->post('hotel_list', 'App\Http\Controllers\Api\Common@hotel_list');
      $api->post('hotels_list', 'App\Http\Controllers\Api\Common@hotels_list');
      $api->post('update_promocode', 'App\Http\Controllers\Api\Ordercheckout@update_promocode');
      $api->post('signup_guest', 'App\Http\Controllers\Api\Common@signup_guest');
      $api->post('verify_guest_otp', 'App\Http\Controllers\Api\Ordercheckout@verify_guest_otp');
      $api->post('order_detail', 'App\Http\Controllers\Api\Ordercheckout@order_detail');
      $api->post('cancel_booking', 'App\Http\Controllers\Api\Ordercheckout@cancel_booking');
      $api->post('signup_guest_now', 'App\Http\Controllers\Api\Common@signup_guest_now');
      $api->post('payment_details_page', 'App\Http\Controllers\Api\Common@payment_details_page');
      $api->post('booking_payment_response','App\Http\Controllers\Api\Common@booking_payment_response');
      $api->post('response_booking_payment','App\Http\Controllers\Api\Common@response_booking_payment');
      $api->post('orders','App\Http\Controllers\Api\Account@orders');
      $api->post('user_detail', 'App\Http\Controllers\Api\Account@user_detail');
      $api->post('update_profile', 'App\Http\Controllers\Api\Account@update_profile');
      $api->post('update_profile_image', 'App\Http\Controllers\Api\Account@update_profile_image');
      $api->post('update_password', 'App\Http\Controllers\Api\Account@update_password');
      $api->post('signup_fb_user', 'App\Http\Controllers\Api\Account@signup_fb_user');
      $api->post('signup_gplus_user', 'App\Http\Controllers\Api\Account@signup_gplus_user');
      $api->post('user_rating', 'App\Http\Controllers\Api\Account@user_rating');
      //    $api->post('wallet','App\Http\Controllers\Api\Common@wallet');
      $api->post('user_wallet', 'App\Http\Controllers\Api\UserWallet@user_wallet');
      
   });
