<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DB;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Image;
use MetaTag;
use Mail;
use File;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Hash;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use URL;
use App\Model\admin_customers as Admincustomer;

class CustomerController extends Controller
{
    
     public function __construct(){

     }
  
     /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyAjaxCustomers()
    { 
        $customers=DB::table('admin_customers')->select('*')->orderBy('firstname', 'asc')->get();

        return Datatables::of($customers)->addColumn('action', function ($customers) {
          if(has_permission('admin/customers/edit')) {
                return '<div class="btn-group"><a href="'.URL::to("admin/customers/edit/".$customers->id).'" class="btn btn-xs btn-white" title="'.trans("messages.Edit").'"><i class="fa fa-edit"></i>&nbsp;'.trans("messages.Edit").'</a>
                        <button type="button" class="btn btn-xs btn-white dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                        <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu xs pull-right" role="menu">
                        <li><a href="'.URL::to("admin/customers/view/".$customers->id).'"  title="'.trans("messages.View").'"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;'.@trans("messages.View").'</a></li>
                        </li>
                        <li><a href="'.URL::to("admin/customers/delete/".$customers->id).'" class="delete-'.$customers->id.'" title="'.trans("messages.Delete").'"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;'.@trans("messages.Delete").'</a></li>
                        </ul>
                    </div><script type="text/javascript">
                    $( document ).ready(function() {
                    $(".delete-'.$customers->id.'").on("click", function(){
                         return confirm("'.trans("messages.Are you sure want to delete?").'");
                    });});</script>';
                }
            })
            ->editColumn('firstname', function ($customers) {
                $data = '-';
                if($customers->firstname != null && $customers->lastname != null):
              $data = $customers->firstname.' '.$customers->lastname;
                else:
              $data = $customers->firstname;
                endif;
                return $data;
            })
            ->editColumn('mobile_number', function ($customers) {
                $data = '-';
                if($customers->mobile_number != null && $customers->country_code != null):
              $data = $customers->country_code.'-'.$customers->mobile_number;
                else:
              $data = $customers->mobile_number;
                endif;
                return $data;
            })
             ->editColumn('login_type', function ($customers) {
                    $data = '-';
                    if($customers->status != null)
                    {
                      if($customers->login_type == 1){ 
                        $data = "Web";
                       }elseif($customers->login_type == 2){ 
                        $data = "Mobile";
                       }
                    } 
                    return $data;
            })
            ->editColumn('status', function ($customers) {
                    $data = '-';
                    if($customers->status != null)
                    {
                      if($customers->status == 0){ 
                        $data = "<span class='label label-warning'>Inactive</span>";
                       }elseif($customers->status == 1){ 
                        $data = "<span class='label label-success'>Active</span>";
                       }elseif($customers->status == 2){
                        $data = "<span class='label label-danger'>Deleted</span>";
                       }
                    } 
                    return $data;
            })
            ->editColumn('is_verified', function ($customers) {
                    $data = '-';
                    if($customers->is_verified != null)
                    {
                      if($customers->is_verified == 0){ 
                        $data = "<span class='label label-warning'>Not Verified</span>";
                       }elseif($customers->is_verified == 1){ 
                        $data = "<span class='label label-success'>Verified</span>";
                       }
                    } 
                    return $data;
            })
            ->make(true);
    }

    /**
     * Show the Customer List.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guest())
        {
                return redirect()->guest('admin/login');
        } else {
            return view('admin.customers.list');
        }
        
    }

    /**
     * Show the Customer Add Form.
     *
     */
    public function create()
    {
        if (Auth::guest())
        {
            return redirect()->guest('admin/login');
        } else {
            return view('admin.customers.create');
        }
    }

    /**
     * Storing the Customer Details.
     *
     */
    public function store(Request $data)
    {

        if (Auth::guest()){
            return redirect()->guest('admin/login');
        } else {
              $validation = Validator::make($data->all(),array(
                  'first_name' => 'required|alpha',
                  'last_name' => 'required|alpha',
                  'email' => 'required|unique:admin_customers,email',
                  'password' => 'required',
                  'date_of_birth' => 'required',
                  'mobile' => 'required|numeric|digits_between:6,16',
                  'gender' => 'required',
                  'login_type' => 'required',
                  'address' => 'required|alpha',
                  'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:2048',
              ));   
              $country_short = Input::get('country_short');
              // process the validation
              if ($validation->fails()) { 
                  return Redirect::back()->withErrors($validation)->withInput()->with('country_short',$country_short);
              } else {

                  $add_customers = new Admincustomer();
                  $add_customers->firstname = Input::get('first_name');
                  $add_customers->lastname = Input::get('last_name');
                  $add_customers->email = Input::get('email');
                  $add_customers->hash_password = md5(Input::get('password'));
                  $add_customers->mobile_number = Input::get('mobile');
                  $add_customers->country_code = Input::get('country_code');
                  $add_customers->date_of_birth = Input::get('date_of_birth');
                  $add_customers->gender = Input::get('gender');
                  $add_customers->login_type = Input::get('login_type');
                  $add_customers->address = Input::get('address');
                  $add_customers->status = (int)Input::get('status');
                  $add_customers->is_verified = (int)Input::get('is_verified');
                  $add_customers->customer_unique_id = getRandomBookingNumber(8);
                  $add_customers->otp_verify_status = 1;  // OTP VERIFY STATUS
                  $add_customers->save();

                  //For Customer Image
                  if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                      $destinationPath = base_path().'/public/assets/admin/base/images';
                      $imageName = $_FILES['image']['name'];
                      $data->file('image')->move($destinationPath, $imageName);
                      $destinationPath1 = url('/assets/admin/base/images/'.$imageName.'');
                      Image::make( $destinationPath1 )->fit(75, 75)->save(base_path() .'/public/assets/admin/base/images/customers/'.$imageName)->destroy();
                      unlink($destinationPath.'/'.$imageName);
                      $add_customers->image = $imageName;
                      $add_customers->save();
                  }
                  Session::flash('message', trans('messages.Customer Details has been submitted successfully!'));
                  return Redirect::to('admin/customers');
              }
        }
    }


    /**
     * Customer Details For Editing.
     *
     */
    public function edit($id){
        if (Auth::guest())
        {
            return redirect()->guest('admin/login');
        } else {
            $edit_customer = Admincustomer::find($id);
            if(count($edit_customer) > 0){
              return view('admin.customers.edit')->with('data',$edit_customer);
            } else {
              Session::flash('message', trans('messages.Invalid Customer Details!'));
              return Redirect::to('admin/customers');
            }
            
        }
    }


     /**
     * Updating Customer Details.
     *
     */
    public function update(Request $data,$id){
      if (Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
         $validation = Validator::make($data->all(),array(
            'first_name' => 'required|alpha',
            'last_name' => 'required|alpha',
            'email' => 'required|unique:admin_customers,email,'.$id.',id',
            'date_of_birth' => 'required',
            'mobile' => 'required|numeric|digits_between:6,16',
            'gender' => 'required',
            'login_type' => 'required',
            'address' => 'required|alpha',
            'image' => 'nullable|mimes:jpeg,jpg,png,gif|max:2048',
        ));   
                // process the validation
        if ($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        } else {

            $update_customer = Admincustomer::find($id);
            $update_customer->firstname = Input::get('first_name');
            $update_customer->lastname = Input::get('last_name');
            $update_customer->email = Input::get('email');
            if(Input::get('password') != ""){
              $update_customer->hash_password = md5(Input::get('password'));  
            }
            $update_customer->mobile_number = Input::get('mobile');
            $update_customer->country_code = Input::get('country_code');
            $update_customer->date_of_birth = Input::get('date_of_birth');
            $update_customer->gender = Input::get('gender');
            $update_customer->login_type = Input::get('login_type');
            $update_customer->address = Input::get('address');
            $update_customer->status = (int)Input::get('status');
            $update_customer->is_verified = (int)Input::get('is_verified');
            $update_customer->save();

            //For Customer Image
            if(isset($_FILES['image']['name']) && $_FILES['image']['name']!=''){ 
                $destinationPath = base_path().'/public/assets/admin/base/images';
                $imageName = $_FILES['image']['name'];
                $data->file('image')->move($destinationPath, $imageName);
                $destinationPath1 = url('/assets/admin/base/images/'.$imageName.'');
                Image::make( $destinationPath1 )->fit(75, 75)->save(base_path() .'/public/assets/admin/base/images/customers/'.$imageName)->destroy();
                unlink(url('/assets/admin/base/images/'.$imageName.''));
                $update_customer->image = $imageName;
                $update_customer->save();
            }
            Session::flash('message', trans('messages.Customer Details has been updated successfully!'));
            return Redirect::to('admin/customers');
        }
      }
    }


     /**
     * Deleting Customer Details.
     *
     */
    public function destroy($id){
      if (Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
        $customers = Admincustomer::find($id);
        if(count($customers) > 0){
            $customers->status = 2;
            $customers->save();
            Session::flash('message', trans('messages.Customer has been deleted successfully!'));
            return Redirect::to('admin/customers');
        } else {
             Session::flash('message', trans('messages.Invalid Customer Details!'));
            return Redirect::to('admin/customers');
        }      
      }
    }


    /**
     * View the Customer Details.
     *
     */
    public function show($id){
      if(Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
          $customers = Admincustomer::find($id);
          if(count($customers) > 0){
            $customers = Admincustomer::find($id)->toArray();
            $customer_bookings = DB::table('payments')
                      ->leftjoin('admin_customers','admin_customers.id','=','payments.customer_id')
                      ->leftjoin('outlet_infos','outlet_infos.id','=','payments.property_id')
                      ->where('payments.customer_id','=',$id)
                      ->select('payments.paid_type','payments.paid_amount','payments.description','payments.created_date','admin_customers.firstname','admin_customers.lastname','outlet_infos.outlet_name')->get()->toArray();
            return view('admin.customers.show')->with('data',$customers)->with('bookings',
              $customer_bookings);
          } else {
            Session::flash('message', trans('messages.Invalid Customer Details!'));
            return Redirect::to('admin/customers');
          }
          
      }
    }

    public function referralIndex()
    {
       if(Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
          return view('admin.customers.customer_referrals');
      }
    }

    public function anyAjaxreferralIndex()
    {
      if(Auth::guest())
      {
          return redirect()->guest('admin/login');
      } else {
          $customers = DB::table('booking_referral_logs as bl')
                          ->leftjoin('admin_customers as ac','ac.id',
                            'bl.customer_id')
                          ->select('bl.id','ac.firstname','ac.lastname','ac.email','ac.customer_unique_id',
                            'bl.created_at','ac.referred_customer_id','bl.referral_amount')
                          ->get();
          //echo "<pre>";print_r($customers);die;
          return Datatables::of($customers)
            ->editColumn('first_name', function ($customers) {
                $data = '-';
                if($customers->firstname != null && $customers->lastname != null):
              $data = $customers->firstname.' '.$customers->lastname;
                else:
              $data = $customers->firstname;
                endif;
                return $data;
            })
            ->editColumn('referrer_name', function ($customers) {
                $data = '-';
                if($customers->referred_customer_id != null)
                    $data = getCustomerName($customers->referred_customer_id);
                return $data;
            })
            ->editColumn('referral_code', function ($customers) {
                $data = '-';
                if($customers->referred_customer_id != null)
                    $data = getReferrerCode($customers->referred_customer_id);
                return $data;
            })
            ->make(true);
          
      }
    }
}
