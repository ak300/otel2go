<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use cast_vote\Support\Facades\Validator;
use Illuminate\Support\Facades\URL;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use GuzzleHttp\Client;
use DB;
use App\Model\contactus;
use App\Model\users;
use App\Model\settings;
use App\Model\emailsettings;
use App\Model\cms;
use Session;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Contracts\Auth\Registrar;
use MetaTag;
use Mail;
use SEO;
use SEOMeta;
use OpenGraph;
use Twitter;
use App;
use Cart;
use View;
//use App\Http\Controllers\Api\Api;
use App\Model\api;

class Usercart extends Controller
{
    const USERS_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user_details = array();
        $this->api = New Api;
        $this->client = new Client([
            // Base URI is used with relative requests
            'base_uri' => url('/'),
            // You can set any number of default request options.
            'timeout'  => 3000.0,
        ]);
        $user_details = $this->check_login();
        $this->theme = Session::get("general")->theme;
    }
    
    public function check_login()
    {
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        if(empty($user_id))
        {
            return Redirect::to('/')->send();
        }
        $user_array = array("user_id" => $user_id,"token"=>$token);
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $this->api->call_api($data,'api/user_detail',$method);
        if($response->response->httpCode == 400)
        {
            return Redirect::to('/')->send();
        }
        else
        {
            $this->user_details = $response->response->user_data[0];
            if($this->user_details->email == "")
            {
                Session::flash('message-failure',trans("messages.Please fill your personal details"));
                return Redirect::to('/profile')->send();
            }
            return $this->user_details;
        }
    }

    public function index()
    {
        //get_cart
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        $language = getCurrentLang();
        $user_array = array("user_id" => $user_id,"token"=>$token,"language" =>$language);
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $this->api->call_api($data,'api/get_cart',$method);
       
        if($response->response->httpCode == 400)
        {
            $cart_items = array();
        }
        else
        {
            $cart_items = $response->response->cart_items;
            $total = $response->response->total;
            $sub_total = $response->response->sub_total;
            $tax = $response->response->tax;
            $delivery_cost = $response->response->delivery_cost;
            $tax_amount = $response->response->tax_amount;
        }
        //
       
//dd($cart_items); 

        $groupedCartItems = array();
        foreach ($cart_items as $key => $value) {
            $groupedCartItems[$value->store_id][$value->outlet_id][]= $value;
        }

         //
        //echo '<pre>'; print_r($groupedCartItems); exit;

        SEOMeta::setTitle(Session::get("general_site")->site_name.' - '.'Cart');
        SEOMeta::setDescription(Session::get("general_site")->site_name.' - '.'Cart');
        SEOMeta::addKeyword(Session::get("general_site")->site_name.' - '.'Cart');
        OpenGraph::setTitle(Session::get("general_site")->site_name.' - '.'Cart');
        OpenGraph::setDescription(Session::get("general_site")->site_name.' - '.'Cart');
        OpenGraph::setUrl(URL::to('/'));
        Twitter::setTitle(Session::get("general_site")->site_name.' - '.'Cart');
        Twitter::setSite(Session::get("general_site")->site_name);
        return view('front.'.$this->theme.'.cart')->with("user_details",$this->user_details)->with("cart_items",$cart_items)->with("total",$total)->with("sub_total",$sub_total)->with("tax_amount",$tax_amount)->with("delivery_cost",$delivery_cost)->with("language",$language)->with('groupedCartItems', $groupedCartItems);
    }
    
    public function update_cart(Request $data)
    {
        $post_data = $data->all();
       
        $cart_id = $post_data['cart_id'];
        $cart_detail_id = $post_data['cart_detail_id'];
        $qty = $post_data['qty'];
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        $language = getCurrentLang();
        $user_array = array("user_id" => $user_id,"token"=>$token,"language" =>$language,"cart_id"=>$cart_id,"cart_detail_id"=>$cart_detail_id,"qty"=>$qty);
        $method = "POST";
        $data = array('form_params' => $user_array);
        $response = $this->api->call_api($data,'api/update_cart',$method);

        $carts = $response->response->cart_items;
        $grouped = array();

        foreach($carts as $c)
        {
            $grouped[$c->outlet_id][] = $c;
        }
        // echo '<pre>';
        // print_r($grouped);
        // exit;

        $groupedTotals = array();

        foreach( $grouped as $k=>$grp)
        {
            $outlet_total = 0;

            foreach($grp as $oitem)
            {
                $outlet_total += $oitem->quantity * $oitem->discount_price;
            }

            $groupedTotals[] = array('outlet_id'=>$k, 'outlet_total'=> $outlet_total );

        }

        
        //** for dyn update minicart **//
        $view = View::make('front.'.$this->theme.'.cart_refresh');
        $html =  utf8_encode($view);
        $response->response->html = $html;
        $response->response->groupedTotals = $groupedTotals;
        return response()->json($response->response);
    }
    
    // public function update_cart_grouped(Request $data)
    // {
    //     $post_data = $data->all();
       
    //     $cart_id = $post_data['cart_id'];
    //     $cart_detail_id = $post_data['cart_detail_id'];
    //     $qty = $post_data['qty'];
    //     $user_id = Session::get('user_id');
    //     $token = Session::get('token');
    //     $language = getCurrentLang();
    //     $user_array = array("user_id" => $user_id,"token"=>$token,"language" =>$language,"cart_id"=>$cart_id,"cart_detail_id"=>$cart_detail_id,"qty"=>$qty);
    //     $method = "POST";
    //     $data = array('form_params' => $user_array);
    //     $response = $this->api->call_api($data,'api/update_cart',$method);

    //     echo '<pre>';
    //     print_r($response);
    //     exit;

    //     $carts = $response->response->cart_items;

    //     $grouped = array();

    //     foreach($carts as $c)
    //     {
    //         $grouped[$c->outlet_id][] = $c;
    //     }
    //     $groupTotals = array();
    //     foreach ($carts as $key => $value) {
    //         # code...
    //         $groupTotals[$value->outlet_id]['total'] = '';
    //     }


    //     $view = View::make('front.'.$this->theme.'.cart_refresh');
    //     $html =  utf8_encode($view);
    //     $response->response->html = $html;

    //     return response()->json($response->response);
    // }

    public function add_to_cart()
    {
       return view('front.'.$this->theme.'.cart')->with("user_details",$this->user_details);
    }

    public function add_cart_info(Request $data)
    {
        $post_data = $data->all();
        $qty = $post_data['quantity'];
        //$total_amount = $post_data['total_amount'];
        $product_id = $post_data['product_id'];
        $is_multi_price = $post_data['is_multi_price'];

        if(isset($is_multi_price) && $is_multi_price) 
            $variant_id = !empty($post_data['variant_id'])?$post_data['variant_id']:'';
        else
            $variant_id = '';

        $outlet_id = $post_data['outlet_id'];
        $vendors_id = $post_data['vendors_id'];
        $user_id = Session::get('user_id');
        $token = Session::get('token');
        $language = getCurrentLang();
        $cart_array = array("user_id" => $user_id,"token"=>$token,"language" =>$language,"qty"=>$qty,
            "product_id"=>$product_id,"outlet_id"=>$outlet_id,"vendors_id"=>$vendors_id,
            "is_multi_price"=>$is_multi_price,"variant_id"=>$variant_id);
        // echo '<pre>';
        // print_r($cart_array);
        // exit;
        $method = "POST";
        $data = array('form_params' => $cart_array);
        $response = $this->api->call_api($data,'api/add_to_cart',$method);


        //** for dyn update minicart **//
        $view = View::make('front.'.$this->theme.'.cart_refresh');
        $html =  utf8_encode($view);
        $response->response->html = $html;
    
        return response()->json($response->response);
    }

    public function refresh_cart(Request $data)
    {
        return view('front.'.$this->theme.'.cart_refresh');
    }
}
