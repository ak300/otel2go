<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Session;
use Closure;
use App;


class Translation extends Controller
{
                /**
                 * Create a new controller instance.
                 *
                 * @return void
                 */

                public function changeLocale(Request $request)
                { 
                    
                    $this->validate($request, ['locale' => 'required|in:ar,en']);
                    Session::put('locale', $request->locale);
                    return redirect()->back();
                }
                
                public function translate(Request $request)
                {
                    $messages="";
                    $this->auto_render = false; 
                    if(isset($_POST['text']) && $_POST['text']!=""){
                        $messages = trans('messages.'.$_POST['text']);
                    }
                   echo json_encode($messages);exit;
                   $this->getResponse()->body();
                }
}
