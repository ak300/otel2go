<?php 
namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Dingo\Api\Http\Request;
use Dingo\Api\Http\Response;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
//use PushNotification;
use Illuminate\Support\Facades\Redirect;
use DB;
use App;
use Image;
use URL;
use Illuminate\Support\Contracts\ArrayableInterface;
use Illuminate\Support\Facades\Text;
use App\Model\users;
use App\Model\Users\cards;
use App\Model\Users\address;
use App\Model\api_Model;
use App\Model\vendors;
use App\Model\favorite_vendors;
use App\Model\cart_model;
use App\Model\cart_info;
use Hash;
use Session;

class CartController extends Controller
{
    const USER_SIGNUP_EMAIL_TEMPLATE = 1;
    const USERS_WELCOME_EMAIL_TEMPLATE = 3;
    const USERS_FORGOT_PASSWORD_EMAIL_TEMPLATE = 6;
    const USER_CHANGE_PASSWORD_EMAIL_TEMPLATE = 13;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct(Request $data) {
		$post_data = $data->all();
		if(isset($post_data['language']) && $post_data['language']!='' && $post_data['language']==2)
			   {
				   App::setLocale('ar');
			   }
			   else {
				   App::setLocale('en');
			   }
   }
    
    /*
     * order detail
     */
    public function update_cart(Request $data)
    {
        $post_data = $data->all();
        if($post_data['qty']>0)
        {
            $affected = DB::update('update cart_detail set quantity = ?,updated_at = NOW() where cart_detail_id = ?', array($post_data['qty'],$post_data['cart_detail_id']));
        }
        else
        {
            $affected = DB::update('delete from cart_detail where cart_detail_id = ?', array($post_data['cart_detail_id']));
            $cart_count = DB::table('cart_detail')
                            ->select('cart_detail_id')
                            ->where('cart_id','=',$post_data['cart_id'])
                            ->first(); 
            if(count($cart_count) == 0)
            {
                DB::update('delete from cart where cart_id= ?', array($post_data['cart_id']));
                $result = array("response" => array("httpCode" => 200,"Message" => trans("messages.Your cart is empty now.")));
            }
            
        }
        $cart_items = $this->calculate_cart($post_data['language'],$post_data['user_id']);
        $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.Cart has been updated successfully!"), "cart_items"=>$cart_items['cart_items'],"total"=>$cart_items['total'],"sub_total"=>$cart_items['sub_total'],"minimum_order_amount"=>$cart_items['minimum_order_amount'],"tax"=>$cart_items['tax'],"delivery_cost"=>(double)$cart_items['delivery_cost'],"tax_amount"=>$cart_items['tax_amount']));
        return json_encode($result);
    }
    
    public function get_cart(Request $data)
    { 
        $post_data = $data->all();
        $language_id = $post_data['language'];
        $cart_items = $this->calculate_cart($post_data['language'],$post_data['user_id']);
        
        $result = array("response" => array("httpCode" => 200 , "Message" => "Cart details", "cart_items"=>$cart_items['cart_items'],"total"=>$cart_items['total'],"sub_total"=>$cart_items['sub_total'],"tax"=>$cart_items['tax'],"tax_amount"=>$cart_items['tax_amount'],"outlet_id"=>$cart_items['outlet_id'],"minimum_order_amount"=>$cart_items['minimum_order_amount'],"vendor_id"=>$cart_items['vendor_id'],"delivery_cost"=>(double)$cart_items['delivery_cost']));
        return json_encode($result);
    }

    public function calculate_cart($language,$user_id)
    {
        $cart_data = cart_model::cart_items($language,$user_id); // get all cart items for this user
        //$delivery_settings = $this->get_delivery_settings();
        $sub_total = $tax = $delivery_cost = 0;
        $vendor_id = $outlet_id =  '';
        $minimum_order_amount = getSettings('min_order_amount');
        $minimum_order_amount = !(empty($minimum_order_amount ))? $minimum_order_amount : 0;
        $is_service_tax =  getSettings('is_service_tax_applicable');
        if($is_service_tax)
        {
            $tax = getSettings('service_tax');
        }else
        {
             $tax = 0;
        }

    
        foreach($cart_data as $key=>$items)
        {
            $sub_total += $items->quantity * $items->discount_price;
            //$tax += $items->service_tax; // service tax come from outlet
            
            $product_image = URL::asset('assets/front/'.Session::get("general")->theme.'/images/no_image.png');
            if(file_exists(base_path().'/public/assets/admin/base/images/products/list/'.$items->product_image) && $items->product_image != '')
            {
                $product_image = url('/assets/admin/base/images/products/list/'.$items->product_image);
            }

            //$cart_data[$key]->image_url = $product_image;

            $category_list = getCategoryListsById($items->sub_category_id);
            $cart_data[$key]->sub_category_name = isset($category_list->category_name)?$category_list->category_name:'';

        

            //$category_list = getCategoryListsById($items->sub_category_id);

            $cart_data[$key]->unit = $items->unit;
            $cart_data[$key]->title = $items->title;
            $vendor_id = $items->store_id;
            $outlet_id = $items->outlet_id;
            //$minimum_order_amount = $items->minimum_order_amount;
            //$delivery_settings = $this->get_delivery_settings();
        }
        //print_r($cart_data);exit;
        $tax_amount = $sub_total * $tax /100; 
        $total = $sub_total+$tax_amount;

        if($sub_total > $minimum_order_amount)
        {
            $delivery_cost = 0;
        }else
        {

            $delivery_cost = getSettings('delivery_cost');

            // if($delivery_settings->on_off_status == 1)
            // {
            //     if($delivery_settings->delivery_type == 1)
            //     {
            //         $total = $total+$delivery_settings->delivery_cost_fixed;
            //         $delivery_cost = $delivery_settings->delivery_cost_fixed;
            //     }
            //     if($delivery_settings->delivery_type == 2)
            //     {
            //         $total = $total+$delivery_settings->flat_delivery_cost;
            //         $delivery_cost = $delivery_settings->flat_delivery_cost;
            //     }
                
            // }

        }

        return array("cart_items"=>$cart_data,"total"=>$total,"sub_total"=>$sub_total,"delivery_cost"=>$delivery_cost,"tax"=>$tax,"vendor_id"=>$vendor_id,"outlet_id"=>$outlet_id,"minimum_order_amount"=>$minimum_order_amount,"tax_amount"=>$tax_amount,);
    }


    /*
     * order detail
     */
     public function get_delivery_settings()
    {
        $delivery_settings = DB::table('delivery_settings')->first();
        return $delivery_settings;
    }
    public function add_cart(Request $data)
    {

        $post_data = $data->all();


        if($post_data['language']==2)
        {
            App::setLocale('ar');
        }
        else {
            App::setLocale('en');
        }

        $data= array();
        $rules = [
            'user_id'    => ['required'],
            'vendors_id' => ['required'],
            'outlet_id'  => ['required'],
            'product_id' => ['required'],
            'qty'        => ['required'],
            'token'      => ['required'],
            //'is_multi_price' => ['integer'],
        ];
        $errors = $result = array();
        $validator = app('validator')->make($post_data, $rules);

        if ($validator->fails()) 
        {
            $j = 0;
            foreach( $validator->errors()->messages() as $key => $value) 
            {
                $errors[] = is_array($value)?implode( ',',$value ):$value;
            }
            $errors = implode( ", \n ", $errors );
            $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => $errors));
        }
        else {
            try {
                //$check_auth = JWTAuth::toUser($post_data['token']); 
                $ucdata     = DB::table('cart')
                                ->select('cart.cart_id')
                                ->where("cart.user_id","=",$post_data['user_id'])
                                ->get();

                //check if user cart OBJ exists

                if(count($ucdata))
                {
                    // $uucdata = DB::table('cart')
                    //             ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                    //             ->select('cart.cart_id','cart_detail.product_id','cart_detail.quantity','cart_detail.cart_detail_id')
                    //             ->where("cart.user_id","=",$post_data['user_id'])
                    //             ->where("cart_detail.store_id","=",$post_data['vendors_id'])
                    //             ->where("cart_detail.outlet_id","=",$post_data['outlet_id'])
                    //             ->get();

                    
                    //if(count($uucdata)) // if user had ANY TYPE of product from THIS vendor & outlet
                    //{
                        $cdata = DB::table('cart')
                                    ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                                    ->select('cart.cart_id','cart_detail.product_id','cart_detail.quantity','cart_detail.cart_detail_id',
                                        DB::raw('coalesce(cart_detail.product_variant_id,0)')
                                    )
                                    ->where("cart.user_id","=",$post_data['user_id'])
                                    ->where("cart_detail.store_id","=",$post_data['vendors_id'])
                                    ->where("cart_detail.outlet_id","=",$post_data['outlet_id'])
                                    ->where("cart_detail.product_id","=",$post_data['product_id']);

                        if($post_data['is_multi_price'] == 1)
                        {
                            $cdata = $cdata->where("cart_detail.product_variant_id","=",$post_data['variant_id']);
                        }
                        
                        $cdata = $cdata->get();

                        // get quantity of CURRENT PRODUCT in the cart

                        if(count($cdata))   //If product WAS ALREADY THERE in cart
                        { 
                            
                            $last_quantity = $cdata[0]->quantity;               
                            $cart = Cart_model::find($cdata[0]->cart_id);       
                            $cart->updated_at  = date("Y-m-d H:i:s");           
                            $cart->save();                                      
                            //since we are going to change qty ,also update `cart update time`

                            $cart_info = Cart_info::find($cdata[0]->cart_detail_id); 
                            $qty = $post_data['qty'];                       
                            $cart_info->quantity  = $qty;                     
                            //find row associated with current product update qty

                            if($qty == 0)                                   
                            {
                                //if new qty sent was "0" delete the row in `cart_detail` associated with this row
                                $affected = DB::update('delete from cart_detail where cart_detail_id = ?', array($cdata[0]->cart_detail_id));
                            }else{
                                $cart_info->updated_at  = date("Y-m-d H:i:s"); 
                                $cart_info->save();
                            }

                            $cart_item = 0;

                            // after updating row in `cart_details` count TOTAL NO. OF UNIQUE ITEMS in cart and send to user
                            if($post_data['user_id'])
                            {
                                $cdata = DB::table('cart')
                                        ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                                        ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                                        ->where("cart.user_id","=",$post_data['user_id'])
                                        ->groupby('cart_detail.cart_id')
                                        ->get();
                                // count total products

                                if(count($cdata))
                                {
                                    $cart_item = $cdata[0]->cart_count;
                                }
                            }
                            if($last_quantity > $post_data['qty'])
                            {
                                $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.Cart has been updated successfully!"),"type" => 2,"cart_count" => $cart_item));
                            }
                            else {
                                $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.The product has been added to your cart"),"type" => 2,"cart_count" => $cart_item));
                            }
                        }
                        else  //If product WAS NOT ALREADY THERE in cart , adding FIRST time
                        {

                            $ccdata = DB::table('cart')
                                        ->select('cart.cart_id')
                                        ->where("cart.user_id","=",$post_data['user_id'])
                                        ->get();
// echo '.......';
// print_r($ccdata );
// exit;

                            // get unique cart_id of user for the vendor/store
                            if(count($ccdata))
                            {
                                $cart = Cart_model::find($ccdata[0]->cart_id);
                                $cart->updated_at  = date("Y-m-d H:i:s");
                                $cart->save();
                            }
                            else {   // if not cart_id for this vendor/outlet create one
                                // ** THIS WILL NEVER GET CALL ** //
                                $cart     = new Cart_model;
                                $cart->user_id     = $post_data['user_id'];
                                $cart->store_id    = $post_data['vendors_id'];
                                $cart->outlet_id   = $post_data['outlet_id'];
                                $cart->cart_status = 1;
                                $cart->created_at  = date("Y-m-d H:i:s");
                                $cart->updated_at  = date("Y-m-d H:i:s");
                                $cart->save();
                            }

                            // create a row in `cart_details` for this product
                            $cart_info   = new Cart_info;
                            $cart_info->cart_id    = $cart->cart_id;
                            $cart_info->product_id = $post_data['product_id'];
                            $cart_info->store_id   = $post_data['vendors_id'];
                            $cart_info->outlet_id   = $post_data['outlet_id'];
                            $cart_info->quantity   = $post_data['qty'];

                            if($post_data['is_multi_price'] == 1)
                            {
                                $cart_info->product_variant_id = $post_data['variant_id'];
                            }
                            
                            $cart_info->created_at = date("Y-m-d H:i:s");
                            $cart_info->updated_at = date("Y-m-d H:i:s");
                            $cart_info->save();

                            // after adding row in `cart_details` count TOTAL NO. OF UNIQUE ITEMS in cart and send to user
                            $cart_item = 0;
                            if($post_data['user_id'])
                            {
                                $cdata = DB::table('cart')
                                            ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                                            ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                                            ->where("cart.user_id","=",$post_data['user_id'])
                                            ->groupby('cart_detail.cart_id')
                                            ->get();
                                if(count($cdata))
                                {
                                    $cart_item = $cdata[0]->cart_count;
                                }
                            }
                            $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.The product has been added to your cart"),"type" => 1,"cart_count" => $cart_item));
                        }
                    //}
                    //else //use has cart but in `cart_details` it has id of another vendor & outlet
                    //{
                    //    $cart_item = 0;
                        // just tell user alongwith count of UNIQUE product in cart
                    //    if($post_data['user_id'])
                    //    {
                    //        $cdata = DB::table('cart')
                    //                    ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                    //                    ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                    //                    ->where("cart.user_id","=",$post_data['user_id'])
                    //                    ->groupby('cart_detail.cart_id')
                    //                    ->get();
                    //        if(count($cdata))
                    //        {
                    //            $cart_item = $cdata[0]->cart_count;
                    //        }
                    //    }
                    //    $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.your cart has items from another branch, please choose the same branch to continue"),"type" => 3,"cart_count" => $cart_item));
                    //}
                }
                else 
                {
                    // no cart found , crate new cart and cart_info
                    $cart     = new Cart_model;
                    $cart->user_id     = $post_data['user_id'];
                    //$cart->store_id    = $post_data['vendors_id'];
                    //$cart->outlet_id   = $post_data['outlet_id'];
                    $cart->cart_status = 1;
                    $cart->created_at  = date("Y-m-d H:i:s");
                    $cart->updated_at  = date("Y-m-d H:i:s");
                    $cart->save();
                    
                    $cart_info   = new Cart_info;
                    $cart_info->cart_id     = $cart->cart_id;
                    $cart_info->product_id  = $post_data['product_id'];
                    $cart_info->quantity    = $post_data['qty'];
                    $cart_info->created_at  = date("Y-m-d H:i:s");
                    $cart_info->updated_at  = date("Y-m-d H:i:s");
                    $cart_info->store_id    = $post_data['vendors_id']; //A
                    $cart_info->outlet_id   = $post_data['outlet_id'];  //A

                    if($post_data['is_multi_price'] == 1)
                    {
                        $cart_info->product_variant_id = $post_data['variant_id'];
                    }

                    $cart_info->save();
                    $cart_item = 0;

                    //count of UNIQUE product in cart
                    if($post_data['user_id'])
                    {
                        $cdata = DB::table('cart')
                                    ->leftJoin('cart_detail','cart_detail.cart_id','=','cart.cart_id')
                                    ->select('cart_detail.cart_id',DB::raw('count(cart_detail.cart_detail_id) as cart_count'))
                                    ->where("cart.user_id","=",$post_data['user_id'])
                                    ->groupby('cart_detail.cart_id')
                                    ->get();
                        if(count($cdata))
                        {
                            $cart_item = $cdata[0]->cart_count;
                        }
                    }
                    $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.The product has been added to your cart"),"type" => 1,"cart_count" => $cart_item));
                }
            }
            catch(JWTException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
            catch(TokenExpiredException $e) {
                $result = array("response" => array("httpCode" => 400, "status" => false, "Message" => trans("messages.Kindly check the user credentials")));
            }
        }            
        return json_encode($result,JSON_UNESCAPED_UNICODE);
    }
    public function delete_cart(Request $data)
	{
		$post_data = $data->all();
        if($post_data['qty']>0)
        {
            $affected = DB::update('update cart_detail set quantity = ?,updated_at = NOW() where cart_detail_id = ?', array($post_data['qty'],$post_data['cart_detail_id']));
        }
        else
        {
            $affected = DB::update('delete from cart_detail where cart_detail_id = ?', array($post_data['cart_detail_id']));
            $cart_count = DB::table('cart_detail')
                            ->select('cart_detail_id')
                            ->where('cart_id','=',$post_data['cart_id'])
                            ->first(); 
            if(count($cart_count) == 0)
            {
                DB::update('delete from cart where cart_id= ?', array($post_data['cart_id']));
                $result = array("response" => array("httpCode" => 200,"Message" => trans("messages.Your cart is empty now.")));
            }
            
        }
        $cart_items = $this->calculate_cart($post_data['language'],$post_data['user_id']);
        $result = array("response" => array("httpCode" => 200 , "Message" => trans("messages.Cart has been deleted successfully!"), "cart_items"=>$cart_items['cart_items'],"total"=>$cart_items['total'],"sub_total"=>$cart_items['sub_total'],"minimum_order_amount"=>$cart_items['minimum_order_amount'],"tax"=>$cart_items['tax'],"delivery_cost"=>(double)$cart_items['delivery_cost'],"tax_amount"=>$cart_items['tax_amount']));
        return json_encode($result);
    }
}
