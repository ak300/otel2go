<?php

namespace App\Model;

use Illuminate\Support\Facades\DB;
use Closure;
use Illuminate\Database\Eloquent\Model;


class user_wallets extends Model 
{
    public $timestamps  = false;
	protected $table = 'wallet_logs';
    /* user creditted wallet list */
    public static function get_user_credited_wallet_list($user_id)
    {
        $wallet = DB::table('wallet_logs')
                    ->select('wallet_transaction.total_amount','wallet_logs.created_date','wallet_logs.wallet_id')
                    ->join('wallet_transaction','wallet_transaction.id','=','wallet_logs.wallet_id')
                    ->where('wallet_logs.customer_id','=',$user_id)
                    ->where('wallet_logs.type','=',1)
                    ->orderby('wallet_logs.id','desc')
                    ->get();
        return $wallet;
    }
    /* user debitted wallet list */
    public static function get_user_debitted_wallet_list($user_id)
    {
        $wallet = DB::table('wallet_logs')
                    ->select('wallet_logs.amount','wallet_logs.created_date','wallet_logs.id','wallet_logs.booking_id')
                    ->where('wallet_logs.customer_id','=',$user_id)
                    ->where('wallet_logs.type','=',2)
                    ->orderby('wallet_logs.id','desc')
                    ->get();
        return $wallet;
    }
    /* Payment gateway list */
    public static function get_payment_gateways($language_id)
    {
        $query = 'payment_gateways_info.language_id = (case when (select count(payment_gateways_info.language_id) as totalcount from payment_gateways_info where payment_gateways_info.language_id = '.$language_id.' and payment_gateways.id = payment_gateways_info.payment_id) > 0 THEN '.$language_id.' ELSE 1 END)';
        $gateways = DB::table('payment_gateways')
                ->select('payment_gateways.payment_type','payment_gateways_info.name','payment_gateways.id as payment_gateway_id','payment_gateways.merchant_key','payment_gateways.account_id','payment_gateways.payment_mode','payment_gateways.commision','currencies.currency_code')
                ->leftJoin('payment_gateways_info','payment_gateways_info.payment_id','=','payment_gateways.id')
                ->leftJoin('currencies','currencies.id','=','payment_gateways.currency_id')
                ->where('payment_gateways.active_status','=','1')
                ->where('payment_gateways.payment_mode', '=', 1)
                ->whereRaw($query)->orderBy('payment_gateways.id', 'asc')->first();
        return $gateways;
    }
}
