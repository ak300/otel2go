@extends('layouts.vendors')
@section('content')
<style>
.contentpanel img {
  filter: gray; /* IE6-9 */
  -webkit-filter: grayscale(1); /* Google Chrome, Safari 6+ & Opera 15+ */
    -webkit-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 6px 2px rgba(0,0,0,0.75);
    margin-bottom:20px;
}

.contentpanel img:hover {
  filter: none; /* IE6-9 */
  -webkit-filter: grayscale(0); /* Google Chrome, Safari 6+ & Opera 15+ */
}
.contentpanel .uploadArea{ min-height:150px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
.contentpanel h1, h5{ padding:0px; margin:0px; }
.contentpanel h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
.contentpanel .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
.contentpanel .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
.contentpanel h5{ width:95%; line-height:25px;}
.contentpanel h5, h5 img {  float:left;  }
.contentpanel .invalid { border:1px solid red !important; }
.contentpanel .buttonUpload { display:inline-block; padding: 4px 10px 4px; text-align: center; text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25); background-color: #0074cc; 
.contentpanel -webkit-border-radius: 4px;-moz-border-radius: 4px; border-radius: 4px; border-color: #e6e6e6 #e6e6e6 #bfbfbf; border: 1px solid #cccccc; color:#fff; }
.contentpanel .progress img{ margin-top:7px; margin-left:24px; }
 

</style>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/dataTables.buttons.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/jszip.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/pdfmake.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/vfs_fonts.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/export/buttons.html5.min.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery.isotope.js') }}"></script>

<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/multiupload.js') }}"></script>
<script type="text/javascript">
var config = {
	// Valid file formats
	support : "image/jpg,image/png,image/bmp,image/jpeg,image/gif", 
	form: "demoFiler", // Form ID
	dragArea: "dragAndDropFiles", // Upload Area ID
	uploadUrl: '{{url('vendor/gallery-upload')}}' // Server side file url
	//url = '{{url('postcontactus-sidebar')}}'

	//	location.reload();
}
//Initiate file uploader.
$(document).ready(function()
{

	$("#submitHandler").on("click",function()
	{
		// if ($('#outlet_name').val() == '') 
		// {
		// 	alert('Property name cannot be empty');
		// 	return false;
	 //  	}
		var ext = $('#multiUpload').val().split('.').pop().toLowerCase();
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) 
		{
			// alert('-'+$('#outlet_name option:selected').val()+'-outlet');
			alert('invalid extension!');
			return false;
		}
			//location.reload();
	/* $("#multiUpload").on("click",function()
	 {
	 	alert("hi");
	 	//$("#multiUpload").val('');
	 	//$( ".dfiles" ).remove();	
	 });
	*/

});

	initMultiUploader(config);
});
</script>

<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/admin/base/plugins/export/buttons.dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
	<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendor')</a></li>
				<li>@lang('messages.Property')</li>
			</ul>
			<h4>@lang('messages.Property Images')</h4>
		</div>
	</div><!-- media -->
</div><!-- pageheader -->

 	
<div class="contentpanel">

	<div id="dragAndDropFiles" class="uploadArea">
		<h1>Drop Images Here</h1>

	</div>
	<form name="demoFiler" id="demoFiler" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="file" name="multiUpload" id="multiUpload" multiple accept="image/*" />
		<span class="help-text">Please upload 1024X257 images for better quality</span><br />
		<input type="submit" name="submitHandler" id="submitHandler" class="mt10" value="Upload" />
	</form>
	<div class="progressBar">
		<div class="status"></div>
	</div>
	<div class="clearfix mt20"></div>
	<div class="portfolioContainer">
		<?php 
			$vendor_id    = Session::get('vendor_id');

			$property_id    = Session::get('property_id');
		
			$destinationPath = base_path() .'/public/assets/admin/base/images/vendors/property/'.$property_id.'/';
			$fileurl = url('/assets/admin/base/images/vendors/property/'.$property_id.'/');
			$thumb_fileurl = url('/assets/admin/base/images/vendors/property/'.$property_id.'/thumb/');
			foreach(glob($destinationPath.'*') as $filename)
			{
				if(basename($filename) !="thumb")
				{
					//echo $fileurl.'/'.basename($filename)."<br/>";
					if(!file_exists($fileurl.'/'.basename($filename)))
					{
					if (getimagesize($fileurl.'/'.basename($filename)) !== false) {
					?>
						<div class="col-md-3 col-sm-4 col-xs-6">
							<img class="img-responsive" src="<?php echo $fileurl.'/'.basename($filename);?>" />
							<div class="chat_count_list">
								<a href="javascript:;" data-thumb = <?php echo $destinationPath.'thumb/'.basename($filename);?> data-src="<?php echo $filename; ?>" class="delete_image">delete</a>
							</div>
						</div>
					<?php }
					}
				}
			}?>
	</div>
</div>
<script type="text/javascript">
	// This is only to set an animation for <div> element.
$(window).load(function(){
	var $container = $('.portfolioContainer');
	$container.isotope({
       filter: '*',
       animationOptions: {
           duration: 0,
           easing: 'linear',
           queue: false
       }
	});
	$(".delete_image").on("click",function()
	{
		//console.log($(this).parent().parent().children('img').attr('src'));
		//alert('cominsdf');
		var result = confirm("Want to delete?");
		if (result)
		{
			var token, url, data,image_src,this_div;
			image_path = $(this).data("src");
			thumb = $(this).data("thumb");
			this_div =  $(this);
			image_src = $(this).parent().parent().children('img').attr('src');
			token = $('input[name=_token]').val();
			url = '{{url('vendor/image-deletes')}}'
			data = {_token:token,image_src:image_src,image_path:image_path,thumb:thumb};	
				$.ajax({
					url: url,
					headers: {'X-CSRF-TOKEN': token},
					data: data,
					type: 'POST',
					datatype: 'JSON',
					success: function (data) {
						if (data.status == 200)
						{
							$(this_div.parent().parent().hide());
							$('.portfolioContainer').isotope( 'reloadItems' ).isotope();
							location.reload();
							
					   }
					}
				});		
		}
			  
	});

	
});
</script>
@endsection
