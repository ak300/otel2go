@extends('layouts.vendors')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/jquery-ui-1.10.3.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/plugins/switch/js/bootstrap-switch.min.js') }}"></script> 
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/bootstrap-timepicker.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/bootstrap-timepicker.min.css') }}" media="all" rel="stylesheet" type="text/css" /> 
<link href="{{ URL::asset('assets/admin/base/plugins/switch/css/bootstrap3/bootstrap-switch.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/select2.min.js') }}"></script>

<div class="row">
    <div class="col-md-12 ">
        <!-- Nav tabs -->
        <div class="pageheader">
            <div class="media">
                <div class="pageicon pull-left">
                    <i class="fa fa-home"></i>
                </div>
                <div class="media-body">
                    <ul class="breadcrumb">
                        <li><a href="{{ URL::to('vendors/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Vendors')</a></li>
                        <li>@lang('messages.Subscription Packages')</li>
                    </ul>
                    <h4>@lang('messages.View Subscription Packages')</h4>
                </div>
            </div><!-- media -->
        </div><!-- pageheader -->
        <div class="contentpanel">
            <div class="changes_plans_info">
                <div class="changes_plans_title">
                    <div class="changes_plans_title">
                        <h1>Change Plan</h1>
                        <h5>Choose the right plan for you</h5>
                        <h6> </h6>
                    </div>
                    <div class="changes_plans_list">
                        <form method="POST" action="http://192.168.1.2:1002/subscribe_vendor" accept-charset="UTF-8" class="tab-form attribute_form" id="subscribe_vendor" enctype="multipart/form-data"><input name="_token" type="hidden" value="5siz3AEeoaQL6ld24RBmtkTk3ULbw6j4ell0uPHs"> 
                            <div class="changes_plans_list_midd plans_list">
                                @foreach($data as $key => $val)
                                    <div class="col-md-4">
                                        <div class="list_items1">
                                            <h1>{{ $val->subscription_plan_name }}</h1>
                                            <div class="content_center_alig">
                                                <b>Description :</b><p>{{ $val->description }}</p>
                                            </div>
                                            <b>Price :</b>

                                                @if(getCurrencyPosition()->currency_side == 1)
                                                
                                                        {{ getCurrency()." ".$val->subscription_plan_price }}
                                                @else
                                                        {{ $val->subscription_plan_price." ".getCurrency() }}

                                                @endif                                            
                                            </br>
                                            <b>Discount Price :</b>
                                                @if(getCurrencyPosition()->currency_side == 1)
                                                
                                                        {{ getCurrency()." ".$val->subscription_discount_price }}
                                                @else
                                                        {{ $val->subscription_discount_price." ".getCurrency() }}

                                                @endif                                              
                                            </br>
                                            <b>Duration :</b>{{ $val->subscription_plan_duration}} Days</br>
                                            <b>Packages</b></br>
                                            <ul class="children">
                                            <?php $my_packages = getSubscription_package($val->id);
                                            ?>
                                                @foreach($my_packages as $my_key => $my_val)
                                                    <p>
                                                    @foreach($allpackages as $all_key => $all_val)
                                                        @if($all_val->id == $my_val->package_id)
                                                           <li> {{ $all_val->package_name }}</li>
                                                        @endif
                                                    @endforeach
                                                    </p>    
                                                @endforeach
                                            </ul>
                                            <!-- <a id="checkout_submit" class="btn btn-primary btn-sm checkout-button" href="#" title="Buy Now">Choose plan</a> -->
                                            <button type="button" class="btn btn-primary plan_key" value="{{ $val->id }}">Choose Plan</button>
                                        </div>
                                    </div>
                                @endforeach    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">@lang('messages.Subscription Plan Details')</h4>
            </div>
            <div class="modal-body load_modal">

            </div>          
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
       $('.plan_key').click(function(){
            var plan_id = $(this).val();
            if(plan_id !=""){
                $.ajax({
                    url: '{{ url("vendors/subscription_list") }}',
                    type:'POST',
                    data: { _token: "{{csrf_token()}}",plan_id:plan_id},
                   
                    success:function(data){
                        $('#myModal').modal();
                        $('#myModal').on('shown.bs.modal', function(){
                            $('#myModal .load_modal').html(data);
                        });
                        $('#myModal').on('hidden.bs.modal', function(){
                            $('#myModal .modal-body').data('');
                        });
                   }
                });
            }
       });
    });
</script>
@endsection
