@extends('layouts.admin')
@section('content')
	<script src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/plugins/rateit/src/jquery.rateit.js') ;?>"></script>
	<link href="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/plugins/rateit/src/rateit.css');?>" rel="stylesheet">
<div class="row">	
	<div class="col-md-12 ">
<!-- Nav tabs -->
<div class="pageheader">
<div class="media">
		<div class="pageicon pull-left">
			<i class="fa fa-home"></i>
		</div>
		<div class="media-body">
			<ul class="breadcrumb">
				<li><a href="{{ URL::to('admin/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Admin')</a></li>
				<li><a href="{{ URL::to('admin/reports') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Reports')</a></li>
			</ul>
		
		<h4>@lang('messages.Reports')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->

<div class="contentpanel">

<div class="col-md-12">
    {{ csrf_field() }}
    {{-- <div id="container" style="height: 400px; min-width: 600px"></div> --}}
    <div id="container2" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>

    <script src="http://code.highcharts.com/stock/highstock.js"></script>
    <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>
</div><!-- row -->

<script type="text/javascript">
    
<?php  if(count(json_decode($vendor_count_encoded,1))> 0){ ?>

Highcharts.chart('container2', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Vendors & Users Count'
    },
    xAxis: {
        categories: <?php echo $months_encoded;?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Count'
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr>' +
            '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            //  pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: [
    {
        name: 'Users',
        data: <?php  echo $user_count_encoded; ?>

    },
    {
        name: 'Vendors',
        data: <?php  echo $vendor_count_encoded; ?>

    }    
    ]
});

<?php   } else  {   ?>

     $('#container2').hide();

<?php   }   ?>


</script>


</div>
@endsection


