   <!-- Modal for signIn -->
    <div class="modal fade login" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="singup_info">
        <div class="sign_up">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="$('#sign_in').trigger('reset');">
          <span aria-hidden="true">&times;</span>
        </button>
          <div class="signup_inner_containt">
         <div class="signup_image">
              <img src="<?php echo URL::asset('assets/front/'.Session::get("general")->theme.'/images/otel2go1.png'); ?>"  alt="signin">
              <h4>SIGN IN</h4>
              <i class="signup_line"></i>
            </div>
             {!!Form::open(array('method' => 'post', 'class' => 'tab-form attribute_form', 'id' => 'sign_in' ,'onsubmit'=>'return signin()'));!!}
              <div class="form-group">
                <label>Email/Mobile Number</label>
                <input type="text" name="email_mobile" class="form-control" placeholder="" required>
              </div>
              <div class="form-group">
                <label >Password</label>
                <input type="password" name="password" class="form-control" placeholder="" required>
              </div>
              <div class="row">
                <div class="col-sm">
                  <p class="account">Don't have an Account? <a href="javascript:;" class="sign-up-modal" title="@lang('messages.Signup')" data-toggle="modal" data-target=".signup" onclick="$('#sign_in').trigger('reset');">@lang('messages.Signup')</a>
                   </p>
                </div>
                <div class="col-sm-4">
                  <a class="forgot_acr" href="#" data-toggle="modal" data-target=".forgot-password" onclick="$('#sign_in').trigger('reset');">Forgot password?</a>
                </div>
              </div>
              <div class="button_signup">
                <div class="row">
                  <div class="col-sm-6 col padding_right">
                    <button type="button" class="btn cancel_btn signup_btn" onclick="$('#sign_in').trigger('reset');">Cancel</button>
                  </div>
                  <div class="col-sm-6 col">
                    <button type="submit" class="btn signup_btn signinbtn">Sign In</button>
                  </div>
                </div>
              </div>
              <div class="connect_divider">
                <div class="connect_with">
                  <h6>Or Connect with</h6>
                </div>
              </div>
              <div class="connect_btn">
                <div class="row">
                  <div class="col-sm-6 col padding_right">
                    <div class="facebook_sign">
                      <a href="{{ URL::to('auth/facebook') }}" title="@lang('messages.Signup  with  facebook?')"> <i class="glyph-icon flaticon-facebook-letter-logo"></i></a>
                      <!-- <i class="glyph-icon flaticon-facebook-letter-logo"></i> -->
                    </div>
                  </div>
                  <div class="col-sm-6 col">
                    <div class="google_plus_sign">
                      <a href="{{ URL::to('auth/googleplus') }}" class="google_plus_but" title="@lang('messages.Signup  with  facebook?')"><i class="glyph-icon flaticon-google-plus-logo"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            {!!Form::close();!!}
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $('.forgot_acr,.sign-up-modal,.cancel_btn').click(function(){
      $('.login').modal('hide');
    });
  });    
    function signin(){   
      $this = $("#signinbtn");
      $this.html('Processing....');
      data = $("#sign_in").serializeArray();
      var c_url = '/signin_user';
      token = $('input[name=_token]').val();
      $.ajax({
          url: c_url,
          headers: {'X-CSRF-TOKEN': token},
          data: data,
          type: 'POST',
          datatype: 'JSON',
          success: function (resp)
          {
              $this.button('reset');
              data = resp;
              if(data.httpCode == 200)
              {  
                  $this.html('Sign In');
                  $('.login').modal('hide');
                  toastr.success(data.Message);
                  $('#sign_in').trigger('reset');
                  location.reload();
              }
              else if(data.httpCode == 500)
              {
                  toastr.success(data.Message);
                  $this.html('Sign In');
                  $('.login').modal('hide');
                  $('#sign_in').trigger('reset');
                  $('#verify_otp_id').val(data.user_id);
                  $('.verifycode').modal('show');
              } else {
                    if(data.httpCode == 400)
                    {
                        //  alert('AK');
                        //  alert(data.Message);
                        $.each(data.Message,function(key,val){
                            toastr.warning(val);
                        });
                    } else {
                      //  alert('Ak');
                        toastr.warning(data.Message);
                    }                   
                  $this.html('Sign In');
              }
          },
          error:function(resp)
          {
              $this.button('reset');
              console.log('out--'+data); 
              return false;
          }
      });
      return false;
    }
 

</script>
