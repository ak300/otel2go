<script src="http://malsup.github.com/jquery.form.js"></script>
<div class="common_left">
	<div class="user_profile_sections">
		<div class="click_to_phoyo">
			{!!Form::open(array('url' => ['profile_image'], 'method' => 'post','class'=>'tab-form attribute_form','id'=>'imageform','files'=>true));!!}
				<input type="file" name="image" id="photoimg" />
			{!!Form::close();!!}

			<div id="preview">
				<a href="javascript:;" title="@lang('messages.Change image')">

	                <?php $image = $user_details->image; ?>
	                <?php $file_path = url('/assets/admin/base/images/customers/'.$image); ?>
	                
	                @if (!empty($image) && File::exists(public_path('/assets/admin/base/images/customers/'.$image)))
	                    <img src="<?php echo $file_path;  ?>" alt="" class="hotel_image">
	                @else 
	                    <img src="<?php echo url('assets/admin/base/images/default_avatar_male.jpg');  ?>"  class="img-circle">
	                @endif  
					<div class="select_user_photo"></div>
				</a>
			</div>
		</div>
		<?php if($user_details->firstname != null && $user_details->lastname) { ?>

		<h2>{{ucfirst($user_details->firstname).' '.$user_details->lastname}}</h2>
		<?php } else { ?>
		{{$user_details->email}}
		<?php } ?>
		<h5></h5>
	</div>
	<div class="left_side_tabs">
		<ul class="resp-tabs-list">
			<li class="{{ Request::is('profile*') ? 'active' : '' }}"><a href="{{url('profile')}}"  title="@lang('messages.Edit profile')">@lang('messages.Edit profile')</a></li>
			<li class="{{ Request::is('orders*') ? 'active' : '' }}"><a href="{{url('orders')}}" title="@lang('messages.My bookings')">@lang('messages.My bookings')</a></li>
			<li class="{{ Request::is('refer_friends*') ? 'active' : '' }}"><a href="{{url('refer_friends')}}" title="@lang('messages.Refer Friends')">@lang('messages.Refer Friends')</a></li>
			<li class="{{ Request::is('wallet*') ? 'active' : '' }}"><a href="{{url('wallet')}}" title="@lang('messages.Wallet')">@lang('messages.Wallet')</a></li>			
			<?php /*
			<li class="{{ Request::is('address*') ? 'active' : '' }}"><a href="{{url('address')}}" title="@lang('messages.My Cards/address')">@lang('messages.My address')</a></li> 
			<li class="{{ Request::is('favourites*') ? 'active' : '' }}"><a href="{{url('favourites')}}"  title="@lang('messages.My favourites')">@lang('messages.My favourites')</a></li>
			<li class="{{ Request::is('cart*') ? 'active' : '' }}"><a href="{{url('cart')}}"  title="@lang('messages.My cart')">@lang('messages.My cart')</a></li>
			*/ ?>
			<li class="{{ Request::is('change-password*') ? 'active' : '' }}"><a href="{{url('change-password')}}" title="@lang('messages.Change password')">@lang('messages.Change password')</a></li>
			
			<li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{url('logout')}}" title="@lang('messages.Logout')">@lang('messages.Logout')</a></li>
			<?php /* <li class="<?php echo 'Time: '.date('Y-m-d H:i:s'); echo base_path();"></li> */ ?>


		</ul>
	</div>

</div>

<script>
  $('.listing_header').show();
</script>
