@extends('layouts.front')
@section('content')

<?php 

$currency_side = getCurrencyPosition()->currency_side;
$currency_symbol = getCurrency();

?> 
<section class="store_item_list">
    <div class="container">
            <div class="my_account_section">
                <div id="parentHorizontalTab">
                     <div class="row">
					<div class="col-md-3">
                        @include('front.'.Session::get("general")->theme.'.profile_sidebar')
                    </div>
                    <div class="col-md-9">
                        <div class="right_descript">
                          <div class="resp-tabs-container hor_1">
                            <div class="elections_sections">
                                <div class="tabs effect-3">
                                  <!-- tab-content -->
                                  <div class="tab-content">
                                      <section id="tab-item-1" class="edit_profile">
                                          <div class="payment_info">
                                              <h2 class="pay_title">@lang('messages.My bookings') </h2>

                                              <div id="selector" class="btn-group row" style="margin-left:2%;">
                                                  <button  value="1" id='allorders' name='all-orders' class="btn btn-primary btn-sm activebutton"  type="button" >All</button>
                                                  <button  value="5" id='pendingorders' name='initiated-orders' class="btn btn-sm btn-primary" type="button" >Pending</button>
                                                  <button  value="6" id='walkinorders' name='pending-orders' class="btn btn-sm btn-primary" type="button" >Walk In</button>
											                            <button  value="2" id='confirmedorders' name='completed-orders' class="btn btn-sm btn-primary" type="button" >Checked In</button>
                                                  <button  value="3" id='completedorders' name='completed-orders' class="btn btn-sm btn-primary" type="button" >Checked Out</button>
                                                  <button  value="4" id='cancelledorders' name='cancelled-orders' class="btn btn-sm btn-primary" type="button" >Cancelled</button>
<?php   /*                                                  
                                                  <button  value="5" id='initiatedorders' name='initiated-orders' class="btn btn-sm btn-primary" type="button" >Initiated</button>
                                      
                                                  <button style="color: black;" value="6" id='deliveredorders' name='delivered-orders' class="btn btn-sm btn-primary" type="button" >Denied</button>
                                      
                                                  <button style="color: black;" value="7" id='processedorders' name='processed-orders' class="btn btn-sm btn-primary" type="button" >Processed</button>
                                     
                                                  <button style="color: black;" value="8" id='failedorders' name='failed-orders' class="btn btn-sm btn-primary" type="button" >Missed</button>
                                                  <?php /*<button style="color: black;" value="9" id='chargeback' name='charge-back' class="btn btn-sm btn-primary" type="button" >Charge Back</button>*/ ?>

										                           </div>

                                              <div class="my_account_sections">
                                                <div class="table-responsive">
                                                  <table class="table" id="myTable">
                                                    <thead>
                                                        <tr>
                                                            <th>@lang('messages.Order Id')</th>
                                                            <th>@lang('messages.Hotel Name')</th>
                                                            <th>@lang('messages.Price')</th>
                                                            <th>@lang('messages.Status')</th>
                                                            <th>@lang('messages.Check-in Date')</th>
                                                            <th>@lang('messages.Check-out Date')</th>
                                                            <th>@lang('messages.Created Date')</th>
                                                            <th>@lang('messages.View')</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php // print_r($orders);exit;
                                                        if(count($orders)>0){
                                                            foreach($orders as $order){
                                                                $order_id = encrypt($order->id); ?>
                                                                <tr>
                                                                    <td>{{$order->booking_random_id}}</td>
                                                                    <td>{{$order->outlet_name}}</td>
                                                                    
                                                                    <?php if($currency_side==1){ ?>
                                                                    <td>{{trim(getCurrency()).round($order->charges,2)}}</td>
                                                                    <td>
                                                                      <?php }else{ ?>
                                                                    </td>
                                                                      <td>{{round($order->charges,2).trim(getCurrency())}}</td>
                                                                    <td>
                                                                       <?php } ?> 
                                                                    <p style="color:{{$order->color_code}}">
                                                                    <?php echo trans('messages.'.$order->status_name);?>
                                                                    </p></td>
                                                                    <td>
                                                                      <?php echo date("d M, Y", strtotime($order->check_in_date));
                                                                      ?>
                                                                    </td>
                                                                    <td>
                                                                      <?php echo date("d M, Y", strtotime($order->check_out_date));
                                                                      ?>
                                                                    </td>
                                                                    <td>{{ date('d M, Y', strtotime($order->created_date)) }}</td>
                                                                    
                                                                    <td valign="middle">
                                                                        <a title="@lang('messages.View')" href="{{ URL::to('/order-info/'.$order_id) }}">
                                                                          <img src="{{ URL::asset('assets/front/otel2go/images/eye_icon.png') }}" />
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                        <?php } ?>
                                                        <?php }else { ?>
                                                            <tr>
                                                                <td colspan="10">@lang('messages.No bookings found')</td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                 </table>
                                            </div>
                                          </div>
                                       </div>
                                    </section>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>


   <script>
     $('.listing_header').show();
   </script>
    <script src="{{ URL::asset('assets/front/otel2go/js/popper.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('assets/front/otel2go/js/intlTelInput.min.js') }}"></script>
    
<script>
  $( document ).ready(function() {
$('#selector button').click(function() {
    $(this).addClass('activebutton').siblings().removeClass('activebutton');


      var buttonval=$(this).attr('id');
      var input, filter, table, tr, td, i;

    switch (buttonval) {

      case 'allorders':    input = '';
      break;

      case 'pendingorders':    input = 'Pending';
      break; 

  	  case 'confirmedorders':    input = 'Checked In';
      break;
      // case 'initiatedorders':    input = 'Initiated';
      // break;
      case 'walkinorders':    input = 'Walk In';
      break;
      // case 'deliveredorders':    input = 'Denied';
      // break;
     
      case 'cancelledorders':    input = 'Canceled';
      break;

      case 'completedorders':    input = 'Checked Out';
      break;
      //case 'processedorders':    input = 'Processed';
      //break;
      // case 'chargeback':    input = 'Charge Back';
      // break;
      // case 'failedorders':    input = 'Missed';
      // break;

      default:

    }

myFunction(input);

});


});
function myFunction(input) {
  // Declare variables
  var  filter, table, tr, td, i;
  filter = input.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");

  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      if ($.trim(td.innerHTML.toUpperCase()).indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
  // Loop through all table rows, and hide those who don't match the search query

}
</script>



@endsection
