@extends('layouts.front')

@section('content')
<section class="business_banner banner_leftsec">
	<div class="container">
	<div class="row">
	<div class="col-md-6 col-xs-6 col-sm-6">
	<div class="aligin_center">
	<h1>Make the world your 
workplace </h1>
	<p>Built to offer you home like comfort while you travel around the world. Get the room that matches your personality and aura, at an amazing price.</p>
	</div>
	</div>
	<div class="col-md-6 col-xs-6 col-sm-6">
	<div class="signup_form">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>
      <ul>
        @foreach ($errors->all() as $error)
          <li><?php echo trans('messages.'.$error); ?> </li>
        @endforeach
      </ul>
    </div>
    @endif 

    @if (Session::has('message'))
        <script type="text/javascript">      
          toastr.success('Your Accout is created. Our Otel2go Admin Will Contact You Shortly!..');
        </script>
    @endif

	{!!Form::open(array('url' => 'vendors_signup', 'method' => 'post','class'=>'tab-form attribute_form','id'=>'vendors_signup_form','files' => true));!!}
	  <div class="form-group">
    <label for="exampleInputEmail1">First name</label>
    <input type="text" class="form-control"  placeholder="First name" required name="first_name" value="{{ old('first_name') }}">
  </div>
    <div class="form-group">
    <label for="exampleInputEmail1">Last name</label>
    <input type="text" class="form-control"  placeholder="Last name" required name="last_name" value="{{ old('last_name') }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Hotel name</label>
    <input type="text" class="form-control"  placeholder="Hotel name" required name="vendor_name" value="{{ old('vendor_name') }}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" class="form-control" placeholder="Email" required name="email" value="{{ old('email') }}">
    
  </div>
 <div class="form-group">
    <label for="exampleInputEmail1">Password</label>
    <input type="password" class="form-control" placeholder="Password" required name="password" value="{{ old('password') }}">
  </div>

	  <label class="checkmark_button new_check">
	I agree the  <a href="{{ url('cms/terms-and-conditions') }}">Terms &amp; Conditions </a> of Otel2go.
	<input type="checkbox" name="chkbox" required>
	<span class="checkmark"> </span>
	</label>

	  <div class="signup_butons">
	  <button type="submit" class="btn btn-primary">Register as a business</button>
	  </div>
	{{ Form::close() }}
		</div>
		</div>
		</div>
		</div>
		</section>
	  <section class="home_infomations landing_bussiness_pages">
		<div class="container">

      <div class="row">
        <div class="col-sm-3 col-lg-3 col-xs-3">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/rewards.png') }}" alt="Mate Rewards "/>
          </div>
        </div>
        <div class="col-sm-9 col-lg-9 col-xs-9">
         
          <div class="Intrro_section">
          <h2> Maximize your revenue potential
            </h2>
            <p>
                We committed to ensure your room revenue increase up to
                20% as hotel partnerships. High occupancy percentage is not
                mean anything if the Average Room Rate (ARR) is lower. We
                give our traveler a discount and Promo Code in order to boost
                hotel revenue and to ensure our hotel partners getting more
                profit for the businesses. 
            </p>
          </div>
        
         </div>
      </div>

      <div class="row">
        <div class="col-sm-3 col-lg-3 col-xs-3">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/quality.png') }}" alt="Mate Rewards "/>
          </div>
        </div>
        <div class="col-sm-9 col-lg-9 col-xs-9">
         
          <div class="Intrro_section">
          <h2> Attract Potential International and Local Traveler
            </h2>
            <p>
                As we ensure quality assurance of our listing hotels in our platform with benefits to our traveler with MATE Rewards & Rating System, (*coming soon) will help to generate more booking and revenue to the hotel partner from international traveler beside our local travel.  
            </p>
          </div>
        
         </div>
      </div>

      <div class="row">
        <div class="col-sm-3 col-lg-3 col-xs-3">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/rating.png') }}" alt="Mate Rewards "/>
          </div>
        </div>
        <div class="col-sm-9 col-lg-9 col-xs-9">
         
          <div class="Intrro_section">
          <h2> Transform Your Hotel to Smart Digital Hotel
            </h2>
            <p>
                As we build both cloud-PMS and online aggregator platform we will assist our hotel partners to transform as Digital hotel. We integrated our hotel dashboard to OTAs by online. 
            </p>
          </div>
         </div>
      </div>

               
      <div class="row">
        <div class="col-sm-3 col-lg-3 col-xs-3">
          <div class="list_works">
            <img src="{{ URL::asset('assets/front/otel2go/images/display.png') }}" alt="Mate Rewards "/>
          </div>
        </div>
        <div class="col-sm-9 col-lg-9 col-xs-9">
         
          <div class="Intrro_section">
          <h2> Free Marketing Advertisement in our platform and Social Media
            </h2>
            <p>
              We will promote your hotel in our platform as well social media. We will have highlighted your hotel and “viral” your hotel for marketing and promo.

            </p>
          </div>
         </div>
      </div>
<div class="space">
      </div>
      </div>
      
    
  </section>
  <div class="space">
      </div>
  <script>

    $('.listing_header').show();
    
</script>
  
@endsection
