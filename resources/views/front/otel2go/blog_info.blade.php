      @extends('layouts.front')
      @section('content')
		<?php  
			if(file_exists(base_path().'/public/assets/admin/base/images/blog/914_649/'.$blog->image))
			{
				$image_url = url('/assets/admin/base/images/blog/914_649/'.$blog->image.'');
			} 
			else
			{ 
				$image_url =  URL::asset('assets/admin/base/images/blog_det_bg.jpg');
			} 
		?>
	<input type="hidden" class="getiamge" value="<?php echo $image_url; ?>">
    <section class="banner_sections_inner blog_det">
		<img src="{{$image_url}}" title="Blog image">
    </section>
	<section class="blog_det_admin">
<div class="container">
<div class="row">
<div class="col-md-6 col-xs-6 col-sm-6">
<div class="row">
<div class="col-md-3 col-lg-3 col-sm-3">
<div class="admin_photo">
														<img src=" http://dev.webjobsuk.com/assets/admin/base/images/a2x.jpg ">
												</div>
</div>
<div class="col-md-9 col-lg-9 col-lg-9 col-sm-9">
<div class="admin_photo">
														<div class="admin_inf">
<h3>oFood</h3>
<h4>Admin</h4>
</div>
												</div>
</div>
</div>
</div>
<div class="col-md-6 col-xs-6 col-sm-6">
<div class="footer_social">
						<ul>
													<li><p>Share at</p></li>
																									<li>
                                            <a target="_blank" href="#" title="Facebook" class="face1"><i class="glyph-icon flaticon-facebook-logo-button">
                                            </i></a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="#" title="Linked In" class="linked1">
                                            <i class="glyph-icon flaticon-twitter-logo-button"></i></a>
                                        </li>
										      <li>
                                            <a target="_blank" href="#" title="Twitter" class="twitt1">
                                            <i class="glyph-icon flaticon-linkedin-button"></i></a>
                                        </li>
                                        <li>
                                            <a target="_blank" href="#"><i class="glyph-icon flaticon-google-plus-logo-button"></i></a>
                                        </li>
                                    </ul>
								</div>
</div>
</div>
</div>
    </section>
    <section class="error_sections">
		<div class="container">
			<div class="blog_det_infor">
				<h2>{{ ucfirst($blog->title) }}</h2>
				<?php echo $blog->content; ?>
			</div>
			
			
			<div class="bolg_listing">
<div class="blog_title"><h1>Related blog</h1></div>
<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-6">
				
				<div class="blog_list_img">
				<a title="Hotel" href="https://www.otel2go.com/blog/info/hotel">
                        								<img alt="Hotel" src="https://www.otel2go.com/assets/admin/base/images/blog/list/4.jpeg">
							                        </a>
				</div>
				<div class="blog_list_in">
				<h2><a title="Hotel" href="https://www.otel2go.com/blog/info/hotel">Hotel,</a></h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				<a href="https://www.otel2go.com/blog/info/hotel" title="Continue Reading" class="continue_butt"> <span>→</span> Continue Reading</a>
				</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-6">
				
				<div class="blog_list_img">
				<a title="Hotel" href="https://www.otel2go.com/blog/info/hotel">
                        								<img alt="Hotel" src="https://www.otel2go.com/assets/admin/base/images/blog/list/4.jpeg">
							                        </a>
				</div>
				<div class="blog_list_in">
				<h2><a title="Hotel" href="https://www.otel2go.com/blog/info/hotel">Hotel,</a></h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				<a href="https://www.otel2go.com/blog/info/hotel" title="Continue Reading" class="continue_butt"> <span>→</span> Continue Reading</a>
				</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-6">
				
				<div class="blog_list_img">
				<a title="Hotel" href="https://www.otel2go.com/blog/info/hotel">
                        								<img alt="Hotel" src="https://www.otel2go.com/assets/admin/base/images/blog/list/4.jpeg">
							                        </a>
				</div>
				<div class="blog_list_in">
				<h2><a title="Hotel" href="https://www.otel2go.com/blog/info/hotel">Hotel,</a></h2>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
				<a href="https://www.otel2go.com/blog/info/hotel" title="Continue Reading" class="continue_butt"> <span>→</span> Continue Reading</a>
				</div>
				</div>
				
		
			
			</div>
<div class="space"></div>
</div>
		</div>
	</section>
<script>
      $('.listing_header').show();
</script>
<script>	
$( document ).ready(function() {
	var inputC = $('input.getiamge').val();
	$('.blog_detials_bg').css('background', 'url(' + inputC + ')no-repeat');
	//$('.banner_sections_inner').css('background', 'url(' + inputC + ')no-repeat');

});
</script>
@endsection
