@extends('layouts.managers')
@section('content')
<script type="text/javascript" src="{{ URL::asset('assets/admin/base/js/dataTables.min.js') }}"></script>
<link href="{{ URL::asset('assets/admin/base/css/dataTables.min.css') }}" media="all" rel="stylesheet" type="text/css" />
<div class="pageheader">
<div class="media">
	<div class="pageicon pull-left">
		<i class="fa fa-home"></i>
	</div>
	<div class="media-body">
		<ul class="breadcrumb">
			<li><a href="{{ URL::to('managers/dashboard') }}"><i class="glyphicon glyphicon-home"></i>@lang('messages.Managers')</a></li>
			<li>@lang('messages.Expenses')</li>
		</ul>
		<h4>@lang('messages.Expenses')</h4>
	</div>
</div><!-- media -->
</div><!-- pageheader -->
<div class="contentpanel">

<div class="buttons_block pull-right">
<div class="btn-group mr5">
<a class="btn btn-primary tip" href="{{ URL::to('managers/expenses/create') }}" title="Add New">@lang('messages.Add New')</a>
</div>
</div>

@if (Session::has('message'))
		<div class="admin_sucess_common">
	<div class="admin_sucess">
    <div class="alert alert-info"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">@lang('messages.Close')</span></button>{{ Session::get('message') }}</div>
    </div></div>
@endif

 <table id="city-table" class="table table-striped table-bordered responsive">
    <thead>
        <tr class="headings">
			
            <th>@lang('messages.S.no')</th> 
            <th>@lang('messages.Expenses Name')</th> 
            <th>@lang('messages.Expense Price')</th>     
            <th>@lang('messages.Date')</th> 
            <?php   /*  <th>@lang('messages.Status')</th>   */  ?>
            <th>@lang('messages.Actions')</th>
            
        </tr>
    </thead>
<tbody>
<tr>
<td class="empty-text" colspan="7" style="background-color: #fff!important;">
<div class="list-empty-text"> @lang('messages.No records found.') </div>
</td>
</tr>
</tbody>

</table>
<?php /** echo $cities->render(); */ ?>
</div>

<script>
$(function() {
    $('#city-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('ajaxmanagerexpenses.data') !!}',
        "order": [],
		"columnDefs": [{
		  "targets"  : 'no-sort',
		  "orderable": false,
		  
		}],
        columns: [
			{ data: 'cid', name: 'expenses.cid',orderable: false },
			{ data: 'expense_name', name: 'expense_name' },
            { data: 'expense_cost', name: 'expense_cost' },
            { data: 'expense_date', name: 'expense_date' },
            //  { data: 'default_status', name: 'default_status' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
            
        ],
    });
});
</script>
@endsection
